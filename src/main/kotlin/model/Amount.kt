package model

data class Amount(
        private val amountValue: Int
) {

    fun printAmount(): String = amountValue.toString()
}
