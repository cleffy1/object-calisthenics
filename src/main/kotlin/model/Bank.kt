package model

class Bank() {

    private val balance
    get() = transactionHistory.sum()

    private val transactionHistory = TransactionHistory()

    fun deposit(deposit: Deposit) {
        transactionHistory.add(deposit)
    }

    fun printBalance(): String = balance.printAmount()
}
