package bank.features

import cucumber.api.PendingException
import cucumber.api.java8.En
import model.Amount
import model.Bank
import model.Deposit
import java.time.LocalDate

/**
 *
 * StepDefs
 */
class StepDefs : En {
    private val bank = Bank()

    init {
        Given("a client makes a deposit of {int} on {localdate}") {
            amountValue: Int, date: LocalDate ->
            val amount = Amount(amountValue)
            val deposit = Deposit(date, amount)
            bank.deposit(deposit)
        }
        And("a deposit of {int} on {localdate}"
        ) { arg0: Int, arg1: LocalDate -> throw PendingException() }
        And("a withdrawal of {int} on {localdate}"
        ) { arg0: Int, arg1: LocalDate -> throw PendingException() }
        When("she prints her bank statement") { throw PendingException() }
        Then("she would see") { arg0: String -> throw PendingException() }
    }
}
