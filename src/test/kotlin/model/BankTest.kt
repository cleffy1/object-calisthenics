package model

import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.time.LocalDate

class BankTest {

    private lateinit var bank: Bank

    @Before
    fun initializeBank() {
        bank = Bank()
    }

    @Test
    fun `Depose 1000 should increment amount to 1000`() {
        val deposit = Deposit(amount = Amount(1000), date = LocalDate.now())
        bank.deposit(deposit)
        Assert.assertEquals("1000", bank.printBalance())
    }

    @Test
    fun `Depose 100 then 1000 should increment amount to 1100`() {
        val deposit1 = Deposit(amount = Amount(1000), date = LocalDate.now())
        val deposit2 = Deposit(amount = Amount(100), date = LocalDate.now())
        bank.deposit(deposit1)
        bank.deposit(deposit2)
        Assert.assertEquals("1100", bank.printBalance())
    }
}